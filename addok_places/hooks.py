from addok.config import config

# encoding=utf8
from addok.helpers import import_by_path, iter_pipe
from addok.batch import batch, process_documents


def register_command(subparsers):
    DEFAULT_FILE = "latest.osm.pbf"
    DEFAULT_DB = "latest.db"
    config.FILE_TO_IMPORT = DEFAULT_FILE
    config.DB_TO_IMPORT = DEFAULT_DB
    parser = subparsers.add_parser("places", help="Import places from osm file")
    parser.set_defaults(func=process_places)
    parser.add_argument("--file", help="osm.pbf file", default=DEFAULT_FILE)

    parser_cities = subparsers.add_parser(
        "cities", help="Import cities from " "whosonfirst database"
    )
    parser_cities.set_defaults(func=process_cities)
    parser_cities.add_argument("--file", help="db file", default=DEFAULT_DB)


def preprocess_cities():
    CITIES_PROCESSORS_PATH = [
        "addok_places.processors.get_rows_from_sqlite",
        "addok_places.processors.row_to_doc",
    ]
    CITIES_PROCESSORS = [import_by_path(path) for path in CITIES_PROCESSORS_PATH]
    return iter_pipe(None, CITIES_PROCESSORS)


def process_cities(args):
    print("Import places")
    keys = ["file"]
    for key in keys:
        value = getattr(args, key, None)
        if value:
            print(value)
            config.FILE_TO_IMPORT = value
    batch(preprocess_cities())


def preprocess_places():
    PLACES_PROCESSORS_PATH = [
        "addok_places.processors.get_rows",
        "addok_places.processors.row_to_doc",
    ]
    PLACES_PROCESSORS = [import_by_path(path) for path in PLACES_PROCESSORS_PATH]
    return iter_pipe(None, PLACES_PROCESSORS)


def process_places(args):
    print("Import places")
    keys = ["file"]
    for key in keys:
        value = getattr(args, key, None)
        if value:
            print(value)
            config.FILE_TO_IMPORT = value
    batch(preprocess_places())
