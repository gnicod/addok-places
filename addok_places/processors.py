# -*- coding: utf-8 -*-

import json
import sqlite3
from addok.config import config
from addok.helpers import yielder
from addok_places.osmium.places_handler import PlacesHandler


def get_rows(args):
    handler = PlacesHandler()
    handler.apply_file(config.FILE_TO_IMPORT)
    for row in handler.results:
        if "name" in row and row["name"] is not None:
            yield row


def get_rows_from_sqlite(args):
    conn = sqlite3.connect(config.DB_TO_IMPORT)
    c = conn.cursor()

    def get_parent(id, parent_id, params):
        c_sub = conn.cursor()
        for row in c_sub.execute(
            "SELECT id, parent_id, name, placetype from spr where id=? limit 1",
            (parent_id,),
        ):
            (id, parent_id, name, placetype) = row
            if placetype == "region":
                params["region"] = name
            if placetype == "macroregion":
                params["macroregion"] = name
            get_parent(id, parent_id, params)
        return params

    for row in c.execute(
        'SELECT id, parent_id, name, latitude, longitude, min_latitude, max_latitude, min_longitude, max_longitude FROM spr where placetype in ("locality")'
    ):
        (
            id,
            parent_id,
            name,
            latitude,
            longitude,
            min_latitude,
            max_latitude,
            min_longitude,
            max_longitude,
        ) = row
        p = get_parent(id, parent_id, {
            "name": name,
            "bbox": f'{min_longitude},{min_latitude},{max_longitude},{max_latitude}',
            "lat": latitude,
            "lon": longitude,
        })
        yield p


@yielder
def row_to_doc(row):
    print("row to doc")
    print(json.dumps(row))
    return json.dumps(row)
